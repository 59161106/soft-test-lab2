const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', ()=> {
    context('Test isUserNameValid', ()=> {
        it('Function prototype : boolean isUserNameValid(username: String)', ()=> {
            expect(validate.isUserNameValid('mac')).to.be.true;
        });

        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', ()=> {
            expect(validate.isUserNameValid('ma')).to.be.false;
            expect(validate.isUserNameValid('mac')).to.be.true;
        });

        it('ทุกตัวต้องเป็นตัวเล็ก', ()=> {
            expect(validate.isUserNameValid('Mac')).to.be.false;
            expect(validate.isUserNameValid('mac')).to.be.true;
        });
        
        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', ()=> {
            expect(validate.isUserNameValid('mac123478')).to.be.true;
            expect(validate.isUserNameValid('mac123456789123456789')).to.be.false;
        });
    });
    context('Test isAgeValid', ()=> {
        it('Function prototype : boolean isAgeValid (age: String)', ()=> {
            expect(validate.isAgeValid('18')).to.be.true;
        });

        it('age ต้องเป็นข้อความที่เป็นตัวเลข', ()=> {
            expect(validate.isAgeValid('a')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
        });

        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', ()=> {
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('170')).to.be.false;
            expect(validate.isAgeValid('20')).to.be.true;
        });
    });
    context('Test isPasswordValid', ()=> {
        it('Function prototype : boolean isUserNameValid(password: String)', ()=> {
            expect(validate.isPasswordValid('Aaaaaaaa123!')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', ()=> {
            expect(validate.isPasswordValid('Aaaaaaaa123!')).to.be.true;
            expect(validate.isPasswordValid('Aa123!')).to.be.false;
        });
        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', ()=> {
            expect(validate.isPasswordValid('Aaaaaaaa123!')).to.be.true;
            expect(validate.isPasswordValid('aaaaaaaa123!')).to.be.false;
        });
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', ()=> {
            expect(validate.isPasswordValid('Aaaaaaaa123!')).to.be.true;
            expect(validate.isPasswordValid('A1aaa2a3a!')).to.be.true;
            expect(validate.isPasswordValid('Aaaaaaaaaaa!')).to.be.false;
        });
        it('ต้องมีอักขระ พิเศษ !@#$%^&*()_+|~-=\`{}[]:";<>?,./ อย่างน้อย 1 ตัว', ()=> {
            expect(validate.isPasswordValid('Aaaaaaaa123!')).to.be.true;
            expect(validate.isPasswordValid('Aaaaaaaa123')).to.be.false;
        });
    });
    context('Test isDateValid', ()=> {
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', ()=> {
            expect(validate.isDateValid(31, 12, 2000)).to.be.true;
            expect(validate.isDateValid('a','b','c')).to.be.false;
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', ()=> {
            expect(validate.isDateValid(31, 12, 2000)).to.be.true;
            expect(validate.isDateValid(1, 12, 2000)).to.be.true;
            expect(validate.isDateValid(15, 12, 2000)).to.be.true;
            expect(validate.isDateValid(32, 12, 2000)).to.be.false;
            expect(validate.isDateValid(0, 1, 2000)).to.be.false;
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', ()=> {
            expect(validate.isDateValid(31, 1, 2000)).to.be.true;
            expect(validate.isDateValid(30, 6, 2000)).to.be.true;
            expect(validate.isDateValid(31, 12, 2000)).to.be.true;
            expect(validate.isDateValid(31, 0, 2000)).to.be.false;
            expect(validate.isDateValid(31, 13, 2000)).to.be.false;
        });
        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', ()=> {
            expect(validate.isDateValid(31, 12, 1970)).to.be.true;
            expect(validate.isDateValid(31, 12, 2000)).to.be.true;
            expect(validate.isDateValid(31, 12, 2020)).to.be.true;
            expect(validate.isDateValid(31, 12, 1969)).to.be.false;
            expect(validate.isDateValid(31, 12, 2021)).to.be.false;
        });
        it('เดือนแต่ละเดือนมีจำนวนวันต่างกันตามรายการดังต่อไปนี้', ()=> {
            expect(validate.isDateValid(1, 1, 2000)).to.be.true;
            expect(validate.isDateValid(30, 1, 2000)).to.be.true;
            expect(validate.isDateValid(31, 1, 2000)).to.be.true;
            expect(validate.isDateValid(32, 1, 2000)).to.be.false;
            expect(validate.isDateValid(1, 2, 2000)).to.be.true;
            expect(validate.isDateValid(28, 2, 2000)).to.be.true;
            expect(validate.isDateValid(29, 2, 2000)).to.be.true;
            expect(validate.isDateValid(30, 2, 2000)).to.be.false;
            expect(validate.isDateValid(30, 2, 1900)).to.be.false;
            expect(validate.isDateValid(29, 2, 1900)).to.be.false;
            expect(validate.isDateValid(1, 3, 2000)).to.be.true;
            expect(validate.isDateValid(30, 3, 2000)).to.be.true;
            expect(validate.isDateValid(31, 3, 2000)).to.be.true;
            expect(validate.isDateValid(32, 3, 2000)).to.be.false;
            expect(validate.isDateValid(1, 4, 2000)).to.be.true;
            expect(validate.isDateValid(30, 4, 2000)).to.be.true;
            expect(validate.isDateValid(31, 4, 2000)).to.be.false;
            expect(validate.isDateValid(1, 5, 2000)).to.be.true;
            expect(validate.isDateValid(30, 5, 2000)).to.be.true;
            expect(validate.isDateValid(31, 5, 2000)).to.be.true;
            expect(validate.isDateValid(32, 5, 2000)).to.be.false;
            expect(validate.isDateValid(1, 6, 2000)).to.be.true;
            expect(validate.isDateValid(30, 6, 2000)).to.be.true;
            expect(validate.isDateValid(31, 6, 2000)).to.be.false;
            expect(validate.isDateValid(1, 7, 2000)).to.be.true;
            expect(validate.isDateValid(30, 7, 2000)).to.be.true;
            expect(validate.isDateValid(31, 7, 2000)).to.be.true;
            expect(validate.isDateValid(32, 7, 2000)).to.be.false;
            expect(validate.isDateValid(1, 8, 2000)).to.be.true;
            expect(validate.isDateValid(30, 8, 2000)).to.be.true;
            expect(validate.isDateValid(31, 8, 2000)).to.be.true;
            expect(validate.isDateValid(32, 8, 2000)).to.be.false;
            expect(validate.isDateValid(1, 9, 2000)).to.be.true;
            expect(validate.isDateValid(30, 9, 2000)).to.be.true;
            expect(validate.isDateValid(31, 9, 2000)).to.be.false;
            expect(validate.isDateValid(1, 10, 2000)).to.be.true;
            expect(validate.isDateValid(30, 10, 2000)).to.be.true;
            expect(validate.isDateValid(31, 10, 2000)).to.be.true;
            expect(validate.isDateValid(32, 10, 2000)).to.be.false;
            expect(validate.isDateValid(1, 11, 2000)).to.be.true;
            expect(validate.isDateValid(30, 11, 2000)).to.be.true;
            expect(validate.isDateValid(31, 11, 2000)).to.be.false;
            expect(validate.isDateValid(1, 12, 2000)).to.be.true;
            expect(validate.isDateValid(30, 12, 2000)).to.be.true;
            expect(validate.isDateValid(31, 12, 2000)).to.be.true;
            expect(validate.isDateValid(32, 12, 2000)).to.be.false;
        });
    });
});