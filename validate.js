module.exports = {
    isUserNameValid: function(username){
        if(username.length < 3 || username.length > 15) {
            return false;
        }
        if(username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },
    isAgeValid: function(age){
        x = parseInt(age);
        if(x < 18 || x > 100) {
            return false;
        }

        if(parseInt(age) !== x) {
            return false;
        }

        return true;
    },
    isPasswordValid: function(password) {
        if(password.length < 8) {
            return false;
        }
        if(/(?=(.*[A-Z]){1,})/.test(password) !== true) {
            return false;
        }
        if(/(?=(.*[0-9]){3,})/.test(password) !== true) {
            return false;
        }
        if(/[\W_]/.test(password) !== true) {
            return false;
        }
        return true;
    },
    isDateValid: function(dayp, monthp, yearp) {
        if(Number.isInteger(dayp) && Number.isInteger(monthp) && Number.isInteger(yearp)) {
            day = parseInt(dayp);
        month = parseInt(monthp);
        year = parseInt(yearp);
        if(day < 1 || day > 31) {
            return false;
        }
        if(month < 1 || month > 12) {
            return false;
        }
        if(year < 1970 || year > 2020) {
            return false;
        }
        switch (month) {
            case 1:
                if(day < 1 || day > 31) {
                    return false;
                }
                break;
            case 2:
                if(year % 400 == 0 && year % 100 == 0) {
                    if(day < 1 || day > 29){
                        return false;
                    }
                }else{
                    if(day < 1 || day > 28){
                        return false;
                    }
                }
                break;
            case 3:
                if(day < 1 || day > 31){
                    return false;
                }
                break;
            case 4:
                if(day < 1 || day > 30){
                    return false;
                }
                break;
            case 5:
                if(day < 1 || day > 31){
                    return false;
                }
                break;
            case 6:
                if(day < 1 || day > 30){
                    return false;
                }
                break;
            case 7:
                if(day < 1 || day > 31){
                    return false;
                }
                break;
            case 8:
                if(day < 1 || day > 31){
                    return false;
                }
                break;
            case 9:
                if(day < 1 || day > 30){
                    return false;
                }
                break;
            case 10:
                if(day < 1 || day > 31){
                    return false;
                }
                break;
            case 11:
                if(day < 1 || day > 30){
                    return false;
                }
                break;
            case 12: 
                if(day < 1 || day > 31){
                    return false;
                }
                break; 
        }
        return true;
        }
        else {
            return false;
        }
        
    }
}